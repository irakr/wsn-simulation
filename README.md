# WSN-simulation
Simulation of a routing protocol for Wireless Sensor Networks(WSN).

# Description
The WSN simulator is networ simulator specifically designed for wireless sensor networks simulations.
It is specifically designed for my own research works. The results may or may not be accurate as other
widely renown simulation tools like ns2, netsim, opnet, etc.
It uses a multithreaded approach of running events of a network. It was the best solution that I thought
which could also help in easily producing randomness in the event scheduling.
It takes a configuration file as input of which the format is defined by the software itself.
To know the details of the configuration format checkout the file 'input.config'.
This file is used to create simulation environment according to the user's preferences.


# Implementation
- Core system language: C++.
- Input configuration script: My own format
- Analysis tools: gnuplot, python, etc.

#						ENJOY
